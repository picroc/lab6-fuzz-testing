import { calculateBonuses } from "./bonus-system";
const assert = require("assert");

const PROGRAMS = {
    STANDART: 'Standard',
    PREMIUM: 'Premium',
    DIAMOND: 'Diamond',
    NONSENSE: 'Nonsense'
}

describe('Bonus system tests', () => {
    test('Standart with low amount', (done) => {
        assert.strictEqual(calculateBonuses(PROGRAMS.STANDART, 5000), 1 * 0.05);
        done();
    });

    test('Standart with normal amount', (done) => {
        assert.strictEqual(calculateBonuses(PROGRAMS.STANDART, 25000), 1.5 * 0.05);
        done();
    });

    test('Standart with high amount', (done) => {
        assert.strictEqual(calculateBonuses(PROGRAMS.STANDART, 75000), 2 * 0.05);
        done();
    });

    test('Standart with highest amount', (done) => {
        assert.strictEqual(calculateBonuses(PROGRAMS.STANDART, 115000), 2.5 * 0.05);
        done();
    });

    test('Standart with exact low amount', (done) => {
        assert.strictEqual(calculateBonuses(PROGRAMS.STANDART, 10000), 1.5 * 0.05);
        done();
    });

    test('Standart with exact normal amount', (done) => {
        assert.strictEqual(calculateBonuses(PROGRAMS.STANDART, 50000), 2 * 0.05);
        done();
    });

    test('Standart with exact high amount', (done) => {
        assert.strictEqual(calculateBonuses(PROGRAMS.STANDART, 100000), 2.5 * 0.05);
        done();
    });

    // PREMIUM

    test('Premium with low amount', (done) => {
        assert.strictEqual(calculateBonuses(PROGRAMS.PREMIUM, 5000), 1 * 0.1);
        done();
    });

    test('Premium with normal amount', (done) => {
        assert.strictEqual(calculateBonuses(PROGRAMS.PREMIUM, 25000), 1.5 * 0.1);
        done();
    });

    test('Premium with high amount', (done) => {
        assert.strictEqual(calculateBonuses(PROGRAMS.PREMIUM, 75000), 2 * 0.1);
        done();
    });

    test('Premium with highest amount', (done) => {
        assert.strictEqual(calculateBonuses(PROGRAMS.PREMIUM, 115000), 2.5 * 0.1);
        done();
    });

    test('Premium with exact low amount', (done) => {
        assert.strictEqual(calculateBonuses(PROGRAMS.PREMIUM, 10000), 1.5 * 0.1);
        done();
    });

    test('Premium with exact normal amount', (done) => {
        assert.strictEqual(calculateBonuses(PROGRAMS.PREMIUM, 50000), 2 * 0.1);
        done();
    });

    test('Premium with exact high amount', (done) => {
        assert.strictEqual(calculateBonuses(PROGRAMS.PREMIUM, 100000), 2.5 * 0.1);
        done();
    });

    // DIAMOND

    test('Diamond with low amount', (done) => {
        assert.strictEqual(calculateBonuses(PROGRAMS.DIAMOND, 5000), 1 * 0.2);
        done();
    });

    test('Diamond with normal amount', (done) => {
        assert.strictEqual(calculateBonuses(PROGRAMS.DIAMOND, 25000), 1.5 * 0.2);
        done();
    });

    test('Diamond with high amount', (done) => {
        assert.strictEqual(calculateBonuses(PROGRAMS.DIAMOND, 75000), 2 * 0.2);
        done();
    });

    test('Diamond with highest amount', (done) => {
        assert.strictEqual(calculateBonuses(PROGRAMS.DIAMOND, 115000), 2.5 * 0.2);
        done();
    });

    test('Diamond with exact low amount', (done) => {
        assert.strictEqual(calculateBonuses(PROGRAMS.DIAMOND, 10000), 1.5 * 0.2);
        done();
    });

    test('Diamond with exact normal amount', (done) => {
        assert.strictEqual(calculateBonuses(PROGRAMS.DIAMOND, 50000), 2 * 0.2);
        done();
    });

    test('Diamond with exact high amount', (done) => {
        assert.strictEqual(calculateBonuses(PROGRAMS.DIAMOND, 100000), 2.5 * 0.2);
        done();
    });

    // ZEROES
    test('Zero with low amount', (done) => {
        assert.strictEqual(calculateBonuses(PROGRAMS.NONSENSE, 5000), 1 * 0);
        done();
    });

    test('Zero with normal amount', (done) => {
        assert.strictEqual(calculateBonuses(PROGRAMS.NONSENSE, 25000), 1.5 * 0);
        done();
    });

    test('Zero with high amount', (done) => {
        assert.strictEqual(calculateBonuses(PROGRAMS.NONSENSE, 75000), 2 * 0);
        done();
    });

    test('Zero with highest amount', (done) => {
        assert.strictEqual(calculateBonuses(PROGRAMS.NONSENSE, 115000), 2.5 * 0);
        done();
    });

})
